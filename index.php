<?php

require_once('generate_trigger.php');

$host="localhost";
$user="root";
$password="";
$database='dbeirigasi_New';

$koneksi=mysql_connect($host,$user,$password) or die("Gagal Koneksi");

// shell_exec("SSH develop@103.15.241.245");
// $koneksi = mysql_connect("localhost","develop","b1smillah",3307) or die("Gagal Koneksi");

if (!mysql_select_db($database, $koneksi)) {
    echo 'Could not select database';
    exit;
}

$generate=new generete_trigger([
    'database'=>$database,
    'showTable'=>false,
    'addColumn'=>false,
    'trigger_insert'=>null,
    'trigger_update'=>null,
    'trigger_delete'=>null,
    'trigger_show'=>"DELETE",
    'relation_table'=>TRUE,
    // 'change_name_table'=>["_a_d"=>"_b_d"],
    'change_name_table'=>null,
]);

$generate->genereate([
    // 'table'=>["ALL"],
    'table'=>[
        "Ref_Daerah_Irigasi",
        "Ref_Bendung"
    ],
    'queryKhusus'=>"AND  TABLE_NAME NOT LIKE '%HP_%' AND TABLE_NAME NOT LIKE '%SUM_%' AND TABLE_NAME NOT LIKE '%Log_Activity%' AND TABLE_NAME NOT LIKE '%user%' AND TABLE_NAME NOT LIKE '%Ap_%'",
    'column'=>['Ip'=>'varchar(255) NULL'],
    'trigger_show_even'=>"delete"
]);