<?php

class generete_trigger{

    public function __construct($params){
        $params=( object )$params;
        $this->database=$params->database;
        $this->option=$params;
    }

    function create_table_trigger($tableUtama,$tableTrigger){

        $sql ="SELECT  COUNT(*) as jumlah FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$this->database."' AND TABLE_NAME = '".$tableTrigger."'";
        $query = mysql_query($sql) or die (mysql_error());
        $row = mysql_fetch_array($query);
        if($row['jumlah']<1){
            $sql="CREATE TABLE ".$tableTrigger." LIKE ".$tableUtama;
            $query = mysql_query($sql) or die (mysql_error());

            $sql ="SELECT  COLUMN_NAME,COLUMN_TYPE  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database."' AND TABLE_NAME = '".$tableTrigger."' AND ( COLUMN_KEY = 'PRI' OR EXTRA = 'auto_increment' )";
            $query = mysql_query($sql) or die (mysql_error());
            $columnKey = mysql_fetch_array($query);

            if($columnKey['COLUMN_NAME']){
                $sql="ALTER TABLE ".$tableTrigger." CHANGE COLUMN ".$columnKey['COLUMN_NAME']." ".$columnKey['COLUMN_NAME']." ".$columnKey['COLUMN_TYPE']." NOT NULL,DROP PRIMARY KEY";
                $query = mysql_query($sql) or die (mysql_error());
            }
            $sql="ALTER TABLE ".$tableTrigger." ADD  Id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST";
            $query = mysql_query($sql) or die (mysql_error());

            $sql="ALTER TABLE ".$tableTrigger." ADD ( Ip VARCHAR(100),Tanggal TIMESTAMP )";
            $query = mysql_query($sql) or die (mysql_error());
        }
    }

    function trigger_IU($tableUtama,$update=null){
        $even=!empty($this->option->trigger_insert) ? $this->option->trigger_insert : 'AFTER';
        $insial=(strtoupper($even)=="AFTER") ? '_a_i' : '_b_i';
        if($update){
            $even=!empty($this->option->trigger_update) ? $this->option->trigger_update : 'AFTER';
            $insial=(strtoupper($even)=="AFTER") ? '_a_u' : '_b_u';
        }

        $tableTrigger="Hp_".$tableUtama.$insial;

        $this->create_table_trigger($tableUtama,$tableTrigger);

        $sql="SELECT GROUP_CONCAT( COLUMN_NAME ) AS dataField,GROUP_CONCAT( CONCAT('NEW.', COLUMN_NAME)) AS newField FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database."' AND TABLE_NAME = '".$tableUtama."'";
        $query = mysql_query($sql) or die (mysql_error());
        $dataTableDefault=mysql_fetch_array($query);

        if(!$update){
            $sql="DROP TRIGGER IF EXISTS insert_".$tableUtama;
            $query = mysql_query($sql) or die (mysql_error());

            $sql="CREATE TRIGGER insert_".$tableUtama." AFTER INSERT ON ".$tableUtama."
            FOR EACH ROW
            BEGIN

                INSERT INTO ".$tableTrigger." ( ".$dataTableDefault['dataField']." ) VALUES ( ".$dataTableDefault['newField']." );
            END";

            $query = mysql_query($sql) or die (mysql_error());
            $this->show_trigger($tableUtama,"INSERT");
        }else{
            $sql="SELECT GROUP_CONCAT( CONCAT('Old.', COLUMN_NAME,' <> ','NEW.', COLUMN_NAME) SEPARATOR ' OR ') AS ifField FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database."' AND TABLE_NAME = '".$tableUtama."' AND  COLUMN_NAME NOT LIKE '%Id_User%' AND COLUMN_NAME NOT LIKE '%Ip%' ";
            $query = mysql_query($sql) or die (mysql_error());
            $dataTableIf=mysql_fetch_array($query);

            $sql="DROP TRIGGER IF EXISTS update_".$tableUtama;
            $query = mysql_query($sql) or die (mysql_error());

            $sql="CREATE TRIGGER update_".$tableUtama." AFTER UPDATE ON ".$tableUtama."
            FOR EACH ROW
            BEGIN

                IF ".$dataTableIf['ifField']." THEN

                    INSERT INTO ".$tableTrigger." ( ".$dataTableDefault['dataField']." ) VALUES ( ".$dataTableDefault['newField']." );

                END IF;
            END";
            $query = mysql_query($sql) or die (mysql_error());
            $this->show_trigger($tableUtama,"UPDATE");
        }
    }

    function trigger_Del($tableUtama){
        $even=!empty($this->option->trigger_delete) ? $this->option->trigger_delete : 'BEFORE';
        $insial=(strtoupper($even)=="BEFORE") ? '_b_d' : '_a_d';
        $tableTrigger="Hp_".$tableUtama.$insial;

        $this->create_table_trigger($tableUtama,$tableTrigger);

        $sql="SELECT GROUP_CONCAT( COLUMN_NAME ) AS dataField,GROUP_CONCAT( CONCAT('OLD.', COLUMN_NAME)) AS oldField FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database."' AND TABLE_NAME = '".$tableUtama."'";
        $query = mysql_query($sql) or die (mysql_error());
        $dataTableDefault=mysql_fetch_array($query);

        $sql="DROP TRIGGER IF EXISTS delete_".$tableUtama;
        $query = mysql_query($sql) or die (mysql_error());

        $sql="SELECT TABLE_NAME,COLUMN_NAME,REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '".$this->database."' AND REFERENCED_TABLE_NAME = '".$tableUtama."' ORDER BY TABLE_NAME ";
        $query = mysql_query($sql) or die (mysql_error());

        $tmp=[];
        while ( $row=mysql_fetch_array($query)) {
            $check=!empty($tmp[$row['TABLE_NAME']]) ? $tmp[$row['TABLE_NAME']] : null;
            if(!$check){
                $tmp[$row['TABLE_NAME']]=$row['COLUMN_NAME']." = OLD.".$row['COLUMN_NAME'];
            }else{
                $tmp[$row['TABLE_NAME']].=" AND ".$row['COLUMN_NAME']."= OLD.".$row['COLUMN_NAME'];
            }
        };

        $textQuery="";
        foreach ($tmp as $key => $value) {
            $textQuery.="\n#DELETE FROM ".$key." WHERE ".$value.";";
        }

        $sql="CREATE TRIGGER delete_".$tableUtama." ".$even." DELETE ON ".$tableUtama."\n
        FOR EACH ROW\n
        BEGIN\n
            INSERT INTO ".$tableTrigger." ( ".$dataTableDefault['dataField']." )\n VALUES ( ".$dataTableDefault['oldField']." );
            ".$textQuery."
        END";

        $query = mysql_query($sql) or die (mysql_error());

        $this->show_trigger($tableUtama,"DELETE");
    }

    function show_trigger($tableUtama,$statusEven){
        $sql="SELECT ACTION_STATEMENT FROM INFORMATION_SCHEMA. TRIGGERS WHERE TRIGGER_SCHEMA = '".$this->database."' AND EVENT_OBJECT_TABLE = '".$tableUtama."' and EVENT_MANIPULATION = '".$statusEven."'";

        $query = mysql_query($sql) or die (mysql_error());
        $showTrigger=mysql_fetch_array($query);
        if($showTrigger){
            echo "<table>";
                echo "<tr>";
                    echo "<td>".$tableUtama."</td>";
                    echo "<td><textarea rows='20' cols='200'>".$showTrigger['ACTION_STATEMENT']."</textarea></td>";
                echo "</tr>";
            echo "</table>";
            echo "<br>";
            echo "<br>";
        }else{
            echo "Tidak Ada Pada Table ".$tableUtama;
        }
    }

    function show_relation($tableUtama){
        $sql="SELECT TABLE_NAME,COLUMN_NAME,REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '".$this->database."' AND REFERENCED_TABLE_NAME = '".$tableUtama."' ORDER BY TABLE_NAME ";
        $query = mysql_query($sql) or die (mysql_error());

        echo "<div>=============== ".$tableUtama." ===============</div>";
        while ( $row=mysql_fetch_array($query)) {
            echo "<table>";
                echo "<tr>";
                    echo "<td>TABLE_NAME</td>";
                    echo "<td>&nbsp;&nbsp;&nbsp;".$row['TABLE_NAME']."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>COLUMN_NAME</td>";
                    echo "<td>&nbsp;&nbsp;&nbsp;".$row['COLUMN_NAME']."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>REFERENCED_TABLE_NAME</td>";
                    echo "<td>&nbsp;&nbsp;&nbsp;".$row['REFERENCED_TABLE_NAME']."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>REFERENCED_COLUMN_NAME</td>";
                    echo "<td>&nbsp;&nbsp;&nbsp;".$row['REFERENCED_COLUMN_NAME']."</td>";
                echo "</tr>";
            echo "</table>";
        };
        echo "<br>";
        echo "<br>";
    }

    function add_Column($data=[]){
        $dataTable=$data['data'];
        $dataColumn=!empty($data['column']) ? $data['column'] : null;

        foreach ($dataTable as $keyT => $valueT) {
            foreach ($dataColumn as $keyC => $valueC) {
                $sql ="SELECT  COUNT(*) as jumlah FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database."' AND TABLE_NAME = '".$valueT."' AND COLUMN_NAME = '".$keyC."' ";
                $query = mysql_query($sql) or die (mysql_error());
                $row = mysql_fetch_array($query);
                if(!$row["jumlah"]){
                    $sql ="ALTER TABLE ".$valueT." ADD ".$keyC." ".$valueC;
                    var_dump($sql);
                    $query = mysql_query($sql) or die (mysql_error());
                }else{
                    var_dump($keyC." Sudah ada Pada Table ".$valueT );
                }
            }
        }
    }

    function change_table($tableUtama){
        $dataChange=$this->option->change_name_table;
        foreach ($dataChange as $key => $value) {
            $sql="Alter Table Hp_".$tableUtama.$key." RENAME Hp_".$tableUtama.$value;
            $query = mysql_query($sql) or null;
            if($query){
                echo "<div>=============== ".$tableUtama." ===============</div>";
                echo "<table>";
                    echo "<tr>";
                        echo "<td>Hp_".$tableUtama.$key."</td>";
                        echo "<td>Hp_".$tableUtama.$value."</td>";
                    echo "</tr>";
                echo "</table>";
                echo "<br>";
                echo "<br>";
            }else{
                echo "<div>=============== ".$tableUtama." Sudah Ready ===============</div>";
                echo "<br>";
                echo "<br>";
            }
        }
    }

    function genereate($data=[]){
        ini_set('max_execution_time',500);
        try {
            $dataTable=$data['table'];
            $queryKhusus=!empty($data['queryKhusus']) ? $data['queryKhusus'] : "" ;
            if(!is_array($dataTable)){
                echo "Format Salah";
            }else{
                if(strtoupper($dataTable[0])==strtoupper('All')){
                    $sql ="SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->database."' ".$queryKhusus." ";

                    $query = mysql_query($sql) or die (mysql_error());
                    while ($row=mysql_fetch_array($query)) {
                        $tableArray[]=$row['TABLE_NAME'];
                    }
                    $dataTable=$tableArray;
                }
            }


            $dataColumn=!empty($data['column']) ? $data['column'] : null;
            if( $dataColumn ){
                if($this->option->addColumn==true){
                    $this->add_Column(['data'=>$dataTable,'column'=>$data['column']]);
                }
            }

            foreach ($dataTable as $keyT => $valueT) {
                if($this->option->showTable==true){
                    var_dump($valueT);
                }

                if($this->option->trigger_insert!=false and $this->option->trigger_insert!=null and $this->option->trigger_insert!=""  ){
                    $this->trigger_IU($valueT);
                }

                if($this->option->trigger_update!=false and $this->option->trigger_update!=null and $this->option->trigger_update!=""  ){
                    $this->trigger_IU($valueT,'u');
                }

                if($this->option->trigger_delete!=false and $this->option->trigger_delete!=null and $this->option->trigger_delete!=""  ){
                    $this->trigger_Del($valueT);
                }
                if($this->option->trigger_show==true and $this->option->trigger_show!=null ){
                    $even='';
                    if(strtoupper($this->option->trigger_show)=='UPDATE'){
                        $even="UPDATE";
                    }elseif(strtoupper($this->option->trigger_show)=='DELETE'){
                        $even="DELETE";
                    }else{
                        $even="INSERT";
                    }
                    $this->show_trigger($valueT,$even);
                }

                if($this->option->relation_table==true){
                    $this->show_relation($valueT);
                }

                if($this->option->change_name_table){
                    $this->change_table($valueT);
                }
            }
        } catch (Exception $e) {
            echo "Gagal";
        }
    }
}